export default {
  breakpoints: ['30em', '48em', '62em', '80em'],
  fonts: {
    heading: '"Avenir Next", sans-serif',
    body: 'system-ui, sans-serif',
    mono: 'Menlo, monospace'
  },
  colors: {
    bg: {
      DEFAULT: '#253140',
      light: '#486480'
    },
    fg: {
      DEFAULT: '#FEFEFE',
    },
    primary: {
      DEFAULT: '#E25354',
      light: '#EE9694',
    }
    /*
    transparent: 'transparent',
    current: 'currentColor',
    blue: {
      lightest: '#BECBDA',
      light: '#486480',
      DEFAULT: '#384A61',
      dark: '#253140',
    },
    red: {
      light: '#EE9694',
      DEFAULT: '#E25354',
    },
    gray: {
      light: '#E6E8E6',
      DEFAULT: '#ABB1AB',
      dark: '#656D78',
    },
    white: {
      DEFAULT: '#FEFEFE',
    },
    brand: {
      50: '#daffff',
      100: '#b1fbfb',
      200: '#85f7f7',
      300: '#58f3f3',
      400: '#31f0f0',
      500: '#1ed7d7',
      600: '#0ca7a7',
      700: '#007777',
      800: '#004949',
      900: '#001a1a',
    }*/
  }
}